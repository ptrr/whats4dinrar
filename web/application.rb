require 'sinatra'
require 'slim'
require 'compass'
require 'builder'
require 'yaml'
require 'cgi'

#require 'index'
#require 'index_reader'
#require 'article_not_found'
#require 'article'
#require 'oldness'
#require 'contents_storage'
#require 'disqus'
#require 'contents'
#require 'archive'
#require 'navigation'
#require 'helpers'

#include Helpers

ROOT = File.expand_path('../..', __FILE__)
set :root, ROOT

Compass.configuration do |config|
  config.project_path = settings.root
  config.sass_dir = 'styles'
  config.output_style = :compressed
  set :sass, Compass.sass_engine_options
end

before do
  #no_www!
  #no_dates!
  #no_trailing_slashes!
  @description = "This is where I write"
end

get '/' do
  slim :index
end
